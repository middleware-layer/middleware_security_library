/**
 ******************************************************************************
 * @file    Criptography_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Criptography/Criptography_stub.hpp>
#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Security
{
   static I_Criptography* p_Criptography_Impl = NULL;

   const char* Criptography_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle Criptography functions";
   }

   void stub_setImpl(I_Criptography* pNewImpl)
   {
      p_Criptography_Impl = pNewImpl;
   }

   static I_Criptography* Criptography_Stub_Get_Impl(void)
   {
      if(p_Criptography_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to Criptography functions. Did you forget"
                   << "to call Stubs::Security::stub_setImpl() ?" << std::endl;

         throw Criptography_StubImplNotSetException();
      }

      return p_Criptography_Impl;
   }

      ///////////////////////////////////////
      // I_Criptography Methods Definition //
      ///////////////////////////////////////

   I_Criptography::~I_Criptography()
   {
      if(p_Criptography_Impl == this)
      {
         p_Criptography_Impl = NULL;
      }
   }

   ///////////////////////////////////////////
   // _Mock_Criptography Methods Definition //
   ///////////////////////////////////////////

   _Mock_Criptography::_Mock_Criptography()
   {
   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   Criptography::Criptography(const Encryption _encryption)
   {
   }

   Criptography::~Criptography()
   {
   }

   const std::vector<uint32_t> Criptography::findDeviceUniqueID(void) const
   {
      return Criptography_Stub_Get_Impl()->findDeviceUniqueID();
   }

   void Criptography::setKey(const std::vector<uint32_t> key)
   {
      Criptography_Stub_Get_Impl()->setKey(key);
   }

   std::vector<uint32_t> Criptography::getKey(void) const
   {
      return Criptography_Stub_Get_Impl()->getKey();
   }
}

