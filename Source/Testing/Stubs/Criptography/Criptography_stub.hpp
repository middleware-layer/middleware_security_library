/**
 ******************************************************************************
 * @file    Criptography.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de mar de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUB_CRIPTOGRAPHY_CRIPTOGRAPHY_HPP
#define SOURCE_TESTING_STUB_CRIPTOGRAPHY_CRIPTOGRAPHY_HPP

#include <Production/Criptography/Criptography.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Security
{
   class Criptography;

   class Criptography_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_Criptography
   {
      public:
         virtual ~I_Criptography();

         virtual const std::vector<uint32_t> findDeviceUniqueID(void) = 0;
         virtual void setKey(const std::vector<uint32_t> key) = 0;
         virtual std::vector<uint32_t> getKey(void) = 0;
   };

   class _Mock_Criptography : public I_Criptography
   {
      public:
         _Mock_Criptography();

         MOCK_CONST_METHOD0(findDeviceUniqueID, std::vector<uint32_t> ());

         MOCK_METHOD1(setKey, void (const std::vector<uint32_t>));
         MOCK_METHOD0(getKey, std::vector<uint32_t> ());
   };

   typedef ::testing::NiceMock<_Mock_Criptography> Mock_Criptography;

   void stub_setImpl(I_Criptography* pNewImpl);
}

#endif // SOURCE_TESTING_STUB_CRIPTOGRAPHY_CRIPTOGRAPHY_HPP
