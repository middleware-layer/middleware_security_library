/**
 ******************************************************************************
 * @file    test_Criptography.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/Criptography/Criptography.hpp>

namespace Security
{
   class CriptographyTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<Criptography> m_Criptography;

      public:
         /**
          * @brief	This method will run always before each test
          */
         void SetUp(void)
         {

         }

         /**
          * @brief	This method will run always after each test
          */
         void TearDown(void)
         {

         }
   };

   TEST_F(CriptographyTest, ConstructorCalled_Success)
   {
   }
}
