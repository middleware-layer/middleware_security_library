/**
 ******************************************************************************
 * @file    Criptography.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de ago de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_CRIPTOGRAPHY_CRIPTOGRAPHY_HPP
#define SOURCE_PRODUCTION_CRIPTOGRAPHY_CRIPTOGRAPHY_HPP

#include <stdint.h>

#include <vector>

namespace Security
{
   /**
    * @brief  This class will handle everything related to criptography
    * 		   Mainly, the system will have the possibility to work with
    * 		   public and private keys (the first for the application and
    * 		   the second for license files
    * 		   The encryption / decryption is done using AES 128 bits
    * 		   The private key is basically the 92 unique device ID bits  + the first 32 bits of it
    * 		   The public key is
    */
   class Criptography
   {
      private:
         /**
          * The key vector is a 128 bits key (for both public and private keys)
          */
         std::vector<uint32_t> m_key;

         /**
          * @brief  Returns the unique device ID for the STM32F7 processor series
          * @retval A vector of 4 x 32 bits integers, representing the Unique Device ID
          */
         const std::vector<uint32_t> findDeviceUniqueID(void) const;

      protected:

      public:
         enum class Encryption
         {
            PUBLIC_KEY,
            PRIVATE_KEY
         };

         /**
          * @brief  The constructor method will only set the encryption / decryption key
          * @param  _encryption  : type of the key
          */
         Criptography(const Encryption _encryption);

         virtual ~Criptography();

         /**
          * @brief  This method will set the criptography system key
          * @param  key    : A vector of 4 x 32 bits integers, representing the Unique Device ID
          */
         void setKey(const std::vector<uint32_t> key);

         /**
          * @brief  This method will return the criptography system key
          * @retval A vector of 4 x 32 bits integers, representing the encryption / decryption system key
          */
         std::vector<uint32_t> getKey(void) const;
   };
}

#endif // SOURCE_PRODUCTION_CRIPTOGRAPHY_CRIPTOGRAPHY_HPP
