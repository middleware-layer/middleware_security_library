/**
 ******************************************************************************
 * @file    Criptography.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    16 de ago de 2017
 * @brief
 ******************************************************************************
 */

#include <Production/Criptography/Criptography.hpp>

namespace Security
{
   Criptography::Criptography(const Encryption _encryption)
   {
      switch (_encryption)
      {
         case Encryption::PUBLIC_KEY:
            break;

         case Encryption::PRIVATE_KEY:
            this->setKey(this->findDeviceUniqueID());

            break;

         default:
            break;
      }
   }

   Criptography::~Criptography()
   {
   }

   const std::vector<uint32_t> Criptography::findDeviceUniqueID(void) const
   {
      std::vector<uint32_t> key;

      /////////////////////////////////////////////////////////
      /// 4 x 32 bits key, repeating the first 32 bits once ///
      /////////////////////////////////////////////////////////

      key.push_back(*reinterpret_cast<uint32_t *>(DEVICE_UID_BASE_ADDRESS));
      key.push_back(*reinterpret_cast<uint32_t *>(DEVICE_UID_BASE_ADDRESS + 4));
      key.push_back(*reinterpret_cast<uint32_t *>(DEVICE_UID_BASE_ADDRESS + 8));
      key.push_back(*reinterpret_cast<uint32_t *>(DEVICE_UID_BASE_ADDRESS));     // It repeats the first 32 bits only to correctly mount the AES key

      return key;
   }

   void Criptography::setKey(const std::vector<uint32_t> key)
   {
      this->m_key = key;
   }

   std::vector<uint32_t> Criptography::getKey(void) const
   {
      return this->m_key;
   }
}
