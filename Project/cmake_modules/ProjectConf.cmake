#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_Security_Library                                          #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

###############################
### GENERAL CONFIGURATIONS  ###
###############################

option(USE_COTIRE                   "Use the COmpilation TIme REducer."                     ON)
option(USE_BOOST                    "Use the Boost Library."                                OFF)
option(STATICLIB_SWITCH             "Compile a statically linked version of the library."   OFF)

##############################
### DEVICE CONFIGURATIONS  ###
##############################

#
# The address inside the Internal Flash storing the Device Unique ID.
# For the STM32F769 processor series, the start address is 0x1FF0F420
#
set(DEFAULT_DEVICE_UID_BASE_ADDRESS                0x1FF0F420)
