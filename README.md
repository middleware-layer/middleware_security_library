# Miidleware_Security_Library

To get the project:
-------------------
Clone the repository: git clone git@gitlab.com:middleware-layer/middleware_security_library.git

To check all available Gradle Tasks:
------------------------------------
	$ gradle tasks --all

Gradle Main Tasks:
-------------------
    $ gradle BuildDocumentation
    $ gradle BuildDebug
    $ gradle BuildRelease
    $ gradle BuildStubs
    $ gradle BuildTests
    $ gradle GenerateReleasePackage
    $ gradle GenerateReleasePackageWithDocumentation
    $ gradle installLibrary
